#!/bin/bash
chmod +x licensetool
mkdir ~/Documents/license/$1
DATE_TODAY=`date +%d/%m/%Y`
./licensetool generate -org $1 -serverid $2 -from ${5:-$DATE_TODAY} -durationType ${4:-"MONTHLY"} -durationPeriod ${6:-"1"} -users ${3:-"-1"} -privatekey /Users/georged/Dropbox/Botron/CMJ/license_certs/licensing.pem > ~/Documents/license/$1/license_$2
./licensetool verify -license ~/Documents/license/$1/license_$2 -publickey /Users/georged/Dropbox/Botron/CMJ/license_certs/licensing_pub.pem
