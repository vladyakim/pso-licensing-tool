package com.botronsoft.pso.licensetool;

import java.io.PrintStream;

/**
 * An executable command.
 */
public interface Command {

	/**
	 * Executes the command.
	 *
	 * @param outputStream
	 *            the output stream where messages can be written.
	 * @throws Exception
	 *             on error.
	 */
	void execute(PrintStream outputStream) throws Exception;
}
