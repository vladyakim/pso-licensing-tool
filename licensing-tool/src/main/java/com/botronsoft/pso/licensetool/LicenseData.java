package com.botronsoft.pso.licensetool;

/**
 * Represents the raw license data as extracted from the license key.
 */
public final class LicenseData {

	private final int version;
	private final String id;
	private final String organization;
	private final String serverId;
	private final long startDate;
	private final long endDate;
	private final int users;

	public LicenseData(int version, String id, String organization, String serverId, long startDate, long endDate, int users) {
		this.version = version;
		this.id = id;
		this.organization = organization;
		this.serverId = serverId;
		this.startDate = startDate;
		this.endDate = endDate;
		this.users = users;
	}

	public int getVersion() {
		return version;
	}

	public String getId() {
		return id;
	}

	public String getOrganization() {
		return organization;
	}

	public String getServerId() {
		return serverId;
	}

	public long getStartDate() {
		return startDate;
	}

	public long getEndDate() {
		return endDate;
	}

	public int getUsers() {
		return users;
	}

}
