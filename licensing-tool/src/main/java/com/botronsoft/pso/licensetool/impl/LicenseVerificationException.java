package com.botronsoft.pso.licensetool.impl;

@SuppressWarnings("serial")
public class LicenseVerificationException extends Exception {

	public LicenseVerificationException(String message) {
		super(message);
	}

}
