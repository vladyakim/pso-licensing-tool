package com.botronsoft.pso.licensetool.impl;

import java.io.File;
import java.nio.ByteBuffer;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.X509EncodedKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;

import com.botronsoft.pso.licensetool.LicenseData;
import com.google.gson.Gson;

class LicenseVerifier {

	private final File publicKeyFile;

	LicenseVerifier(File publicKeyFile) {
		this.publicKeyFile = publicKeyFile;
	}

	public LicenseData verifyLicense(String license) throws Exception {
		ByteBuffer dataBuffer = ByteBuffer.wrap(Base64.decodeBase64(license));
		if (dataBuffer.remaining() < (Integer.SIZE / 8)) {
			throw new LicenseVerificationException("License is maformed!");
		}

		int contentSize = dataBuffer.getInt();
		if ((contentSize <= 0) || (contentSize > dataBuffer.remaining())) {
			throw new LicenseVerificationException("License is maformed!");
		}

		byte[] contentBytes = new byte[contentSize];
		dataBuffer.get(contentBytes, 0, contentBytes.length);

		byte[] signatureBytes = new byte[dataBuffer.remaining()];
		dataBuffer.get(signatureBytes, 0, signatureBytes.length);

		PublicKey pubKey = readPublicKey();

		Signature signature = Signature.getInstance("SHA1withRSA");
		signature.initVerify(pubKey);
		signature.update(contentBytes);

		if (!signature.verify(signatureBytes)) {
			throw new LicenseVerificationException("License is invalid - signature is incorrect!");
		}

		LicenseData licenseData = new Gson().fromJson(new String(contentBytes, "UTF-8"), LicenseData.class);
		return licenseData;
	}

	private PublicKey readPublicKey() throws Exception {
		String publicKey = FileUtils.readFileToString(publicKeyFile);
		publicKey = publicKey.replace("-----BEGIN PUBLIC KEY-----\n", "").replace("-----END PUBLIC KEY-----", "");
		byte[] publicKeyBytes = Base64.decodeBase64(publicKey);

		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		return keyFactory.generatePublic(new X509EncodedKeySpec(publicKeyBytes));
	}

}
