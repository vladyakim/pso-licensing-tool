package com.botronsoft.pso.licensetool;

import java.io.PrintStream;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.spi.SubCommand;
import org.kohsuke.args4j.spi.SubCommandHandler;
import org.kohsuke.args4j.spi.SubCommands;

import com.botronsoft.pso.licensetool.impl.GenerateCommand;
import com.botronsoft.pso.licensetool.impl.LicenseVerificationException;
import com.botronsoft.pso.licensetool.impl.VerifyCommand;

/**
 * The entry point for the license tool.
 */
public class LicenseToolEntryPoint {

	@Argument(index = 0, required = true, usage = "Type of the operation", metaVar = "operation type", handler = SubCommandHandler.class)
	@SubCommands({ @SubCommand(name = "generate", impl = GenerateCommand.class), @SubCommand(name = "verify", impl = VerifyCommand.class) })
	private Command command;

	private final PrintStream outputStream;
	private final PrintStream errorStream;

	public LicenseToolEntryPoint(PrintStream outputStream, PrintStream errorStream) {
		this.outputStream = outputStream;
		this.errorStream = errorStream;
	}

	void execute(String[] args) throws Exception {
		CmdLineParser parser = new CmdLineParser(this);
		parser.getProperties().withUsageWidth(120);

		try {
			// parse the arguments.
			parser.parseArgument(args);
			command.execute(outputStream);
		} catch (LicenseVerificationException e) {
			errorStream.println(e.getMessage());
		} catch (CmdLineException e) {
			errorStream.println(e.getMessage());
			// print the list of available options
			parser.printUsage(errorStream);
			errorStream.println();
		}
	}

	public static void main(String[] args) throws Exception {
		new LicenseToolEntryPoint(System.out, System.err).execute(args);
	}
}
