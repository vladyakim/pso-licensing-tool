package com.botronsoft.pso.licensetool.impl;

import java.io.File;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.kohsuke.args4j.Option;

import com.botronsoft.pso.licensetool.Command;
import com.botronsoft.pso.licensetool.LicenseData;

public class VerifyCommand implements Command {

	@Option(name = "-license", aliases = { "-l" }, usage = "The path to the license file", required = true)
	private File licenseFile;

	@Option(name = "-publickey", usage = "The public key for verifying the license", required = true)
	private File publicKeyFile;

	@Override
	public void execute(PrintStream outputStream) throws Exception {
		String license = FileUtils.readFileToString(licenseFile);

		outputStream.println("Verifying license...");

		LicenseData licenseData = new LicenseVerifier(publicKeyFile).verifyLicense(license);

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateOptionHandler.DATE_FORMAT);

		outputStream.println("Successfully verified license:");
		outputStream.println("Version: " + licenseData.getVersion());
		outputStream.println("License Id: " + licenseData.getId());
		outputStream.println("Organization: " + licenseData.getOrganization());
		outputStream.println("Server ID: " + licenseData.getServerId());
		outputStream.println("Start date: " + simpleDateFormat.format(new Date(licenseData.getStartDate())));
		outputStream.println("End date: " + simpleDateFormat.format(new Date(licenseData.getEndDate())));
		outputStream.println("Users: " + licenseData.getUsers());
	}
}
