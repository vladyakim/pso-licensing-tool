package com.botronsoft.pso.licensetool;

import java.util.concurrent.TimeUnit;

/**
 * The duration of a license can be weekly or monthly.
 */
public enum DurationType {

	WEEKLY(7), MONTHLY(30), YEARLY(365);

	private final int days;

	private DurationType(int days) {
		this.days = days;
	}

	public long toMilliseconds() {
		return TimeUnit.MILLISECONDS.convert(days, TimeUnit.DAYS);
	}
}
