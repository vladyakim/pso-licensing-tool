package com.botronsoft.pso.licensetool.impl;

import java.io.File;
import java.nio.ByteBuffer;
import java.security.KeyFactory;
import java.security.Signature;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.UUID;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;

import com.botronsoft.pso.licensetool.LicenseData;
import com.google.gson.Gson;

public class LicenseGenerator {

	private static final int LICENSE_VERSION = 1;

	private final File privateKeyFile;

	LicenseGenerator(File privateKeyFile) {
		this.privateKeyFile = privateKeyFile;
	}

	public String generateLicense(String organization, String serverId, long startDate, long endDate, int users) throws Exception {
		String id = UUID.randomUUID().toString();
		LicenseData licenseData = new LicenseData(LICENSE_VERSION, id, organization, serverId, startDate, endDate, users);
		String licenseJSON = new Gson().toJson(licenseData);
		byte[] licenseJSONBytes = licenseJSON.getBytes("UTF-8");

		RSAPrivateKey privateKey = readPrivateKey();

		Signature signature = Signature.getInstance("SHA1withRSA");
		signature.initSign(privateKey);
		signature.update(licenseJSONBytes);
		byte[] signatureBytes = signature.sign();

		ByteBuffer licenseBuffer = ByteBuffer.allocate((Integer.SIZE / 8) + licenseJSONBytes.length + signatureBytes.length);
		licenseBuffer.putInt(licenseJSONBytes.length);
		licenseBuffer.put(licenseJSONBytes);
		licenseBuffer.put(signatureBytes);

		String license = new String(Base64.encodeBase64Chunked(licenseBuffer.array()));
		return license;
	}

	private RSAPrivateKey readPrivateKey() throws Exception {
		String privateKey = FileUtils.readFileToString(privateKeyFile);
		privateKey = privateKey.replace("-----BEGIN PRIVATE KEY-----\n", "").replace("-----END PRIVATE KEY-----", "");

		byte[] privateKeyBytes = Base64.decodeBase64(privateKey);

		PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		return (RSAPrivateKey) kf.generatePrivate(privateKeySpec);
	}

}
