package com.botronsoft.pso.licensetool.impl;

import java.io.File;
import java.io.PrintStream;
import java.util.Date;

import org.kohsuke.args4j.Option;

import com.botronsoft.pso.licensetool.Command;
import com.botronsoft.pso.licensetool.DurationType;

public class GenerateCommand implements Command {

	@Option(name = "-org", aliases = { "-o" }, usage = "The name of the organization for which the license is issued", metaVar = "Botron",
			required = true)
	private String organization;

	@Option(name = "-serverid", aliases = { "-s" }, usage = "The server ID", metaVar = "BKPQ-M5IU-F3AD-U6LF", required = true)
	private String serverId;

	@Option(name = "-from", aliases = { "-f" }, usage = "The license start date in the format DD/MM/YYYY", metaVar = "28/02/2015",
			required = true, handler = DateOptionHandler.class)
	private Date fromDate;

	@Option(name = "-durationType", aliases = { "-dt" }, usage = "The duration type of the license", required = true)
	private DurationType durationType;

	@Option(name = "-durationPeriod", aliases = { "-dp" }, usage = "The duration period of the license", required = true)
	private int durationPeriod;

	@Option(name = "-users", aliases = { "-u" }, usage = "The number of users", metaVar = "500", required = true)
	private int users;

	@Option(name = "-privatekey", usage = "The private key for signing the license", required = true)
	private File privateKeyFile;

	@Override
	public void execute(PrintStream outputStream) throws Exception {
		long startDate = fromDate.getTime();
		long endDate = startDate + (durationType.toMilliseconds() * durationPeriod);

		LicenseGenerator generator = new LicenseGenerator(privateKeyFile);
		String license = generator.generateLicense(organization, serverId, startDate, endDate, users);

		outputStream.println(license);
	}
}
