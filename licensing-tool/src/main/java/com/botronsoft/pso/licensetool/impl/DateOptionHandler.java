package com.botronsoft.pso.licensetool.impl;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.OptionDef;
import org.kohsuke.args4j.spi.OneArgumentOptionHandler;
import org.kohsuke.args4j.spi.Setter;

public class DateOptionHandler extends OneArgumentOptionHandler<Date> {

	static final String DATE_FORMAT = "dd/MM/yyyy";

	public DateOptionHandler(CmdLineParser parser, OptionDef option, Setter<? super Date> setter) {
		super(parser, option, setter);
	}

	@Override
	protected Date parse(String argument) throws NumberFormatException, CmdLineException {
		try {
			return new SimpleDateFormat(DATE_FORMAT).parse(argument);
		} catch (java.text.ParseException e) {
			throw new CmdLineException(owner, MessageFormat.format("Date {0} is invalid, must be in the format {1}", argument, DATE_FORMAT),
					e);
		}
	}

}
