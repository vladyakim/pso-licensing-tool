package com.botronsoft.pso.licensetool;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.isEmptyString;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

import java.io.File;
import java.io.PrintStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.junit.Before;
import org.junit.Test;

public class TestLicenseGeneration {

	private LicenseToolEntryPoint entryPoint;
	private ByteArrayOutputStream outputStream;
	private ByteArrayOutputStream errorStream;
	private String privateKeyFilePath;
	private String publicKeyFilePath;

	@Before
	public void setUp() throws Exception {
		outputStream = new ByteArrayOutputStream();
		errorStream = new ByteArrayOutputStream();
		entryPoint = new LicenseToolEntryPoint(new PrintStream(outputStream), new PrintStream(errorStream));

		ClassLoader classLoader = TestLicenseGeneration.class.getClassLoader();
		privateKeyFilePath = new File(classLoader.getResource("test.pem").toURI()).getAbsolutePath();
		publicKeyFilePath = new File(classLoader.getResource("test_pub.pem").toURI()).getAbsolutePath();
	}

	@Test
	public void generateAndVerifyWeeklyLicense() throws Exception {
		//@formatter:off
		String[] generateArgs = new String[] {
				"generate",
				"-org", "Botron",
				"-durationType", "weekly",
				"-durationPeriod", "1",
				"-serverid", "BJRG-HTGY-IRMQ-VY65",
				"-from", "28/02/2015",
				"-users", "500",
				"-privatekey", privateKeyFilePath};
		//@formatter:on

		entryPoint.execute(generateArgs);

		assertThat(outputStream.toString(), not(isEmptyString()));
		assertThat(errorStream.toString(), isEmptyString());

		File tempFile = File.createTempFile("license", ".tmp");
		tempFile.deleteOnExit();

		FileUtils.writeStringToFile(tempFile, outputStream.toString());

		//@formatter:off
		String[] verifyArgs = new String[] {
				"verify",
				"-license", tempFile.getAbsolutePath(),
				"-publickey", publicKeyFilePath};
		//@formatter:on

		outputStream.reset();
		errorStream.reset();

		entryPoint.execute(verifyArgs);

		assertThat(errorStream.toString(), isEmptyString());

		assertThat(outputStream.toString(), not(isEmptyString()));
		assertThat(outputStream.toString(), containsString("Successfully verified license"));
		assertThat(outputStream.toString(), containsString("Version: 1"));
		assertThat(outputStream.toString(), containsString("Organization: Botron"));
		assertThat(outputStream.toString(), containsString("Server ID: BJRG-HTGY-IRMQ-VY65"));
		assertThat(outputStream.toString(), containsString("Start date: 28/02/2015"));
		assertThat(outputStream.toString(), containsString("End date: 07/03/2015"));
		assertThat(outputStream.toString(), containsString("Users: 500"));
	}

	@Test
	public void generateAndVerifyMonthlyLicense() throws Exception {
		//@formatter:off
		String[] generateArgs = new String[] {
				"generate",
				"-org", "Botron",
				"-durationType", "monthly",
				"-durationPeriod", "1",
				"-serverid", "BJRG-HTGY-IRMQ-VY65",
				"-from", "28/02/2015",
				"-users", "500",
				"-privatekey", privateKeyFilePath};
		//@formatter:on

		entryPoint.execute(generateArgs);

		assertThat(outputStream.toString(), not(isEmptyString()));
		assertThat(errorStream.toString(), isEmptyString());

		File tempFile = File.createTempFile("license", ".tmp");
		tempFile.deleteOnExit();

		FileUtils.writeStringToFile(tempFile, outputStream.toString());

		//@formatter:off
		String[] verifyArgs = new String[] {
				"verify",
				"-license", tempFile.getAbsolutePath(),
				"-publickey", publicKeyFilePath};
		//@formatter:on

		outputStream.reset();
		errorStream.reset();

		entryPoint.execute(verifyArgs);

		assertThat(errorStream.toString(), isEmptyString());

		assertThat(outputStream.toString(), not(isEmptyString()));
		assertThat(outputStream.toString(), containsString("Successfully verified license"));
		assertThat(outputStream.toString(), containsString("Version: 1"));
		assertThat(outputStream.toString(), containsString("Organization: Botron"));
		assertThat(outputStream.toString(), containsString("Server ID: BJRG-HTGY-IRMQ-VY65"));
		assertThat(outputStream.toString(), containsString("Start date: 28/02/2015"));
		assertThat(outputStream.toString(), containsString("End date: 30/03/2015"));
		assertThat(outputStream.toString(), containsString("Users: 500"));
	}

	@Test
	public void generateNegativeWrongCommand() throws Exception {
		//@formatter:off
		String[] generateArgs = new String[] {
				"asdsd"
				};
		//@formatter:on

		entryPoint.execute(generateArgs);

		assertThat(outputStream.toString(), isEmptyString());
		assertThat(errorStream.toString(), containsString("is not a valid value"));
	}

	@Test
	public void generateNegativeWrongDateFormat() throws Exception {
		//@formatter:off
		String[] generateArgs = new String[] {
				"generate",
				"-org", "Botron",
				"-durationType", "monthly",
				"-durationPeriod", "1",
				"-serverid", "BJRG-HTGY-IRMQ-VY65",
				"-from", "28.02.2015",
				"-users", "500",
				"-privatekey", privateKeyFilePath};
		//@formatter:on

		entryPoint.execute(generateArgs);

		assertThat(outputStream.toString(), isEmptyString());
		assertThat(errorStream.toString(), containsString("Date 28.02.2015 is invalid, must be in the format dd/MM/yyyy"));
	}

	@Test
	public void verifyNegativeWrongKey() throws Exception {
		//@formatter:off
		String[] generateArgs = new String[] {
				"generate",
				"-org", "Botron",
				"-durationType", "monthly",
				"-durationPeriod", "1",
				"-serverid", "BJRG-HTGY-IRMQ-VY65",
				"-from", "28/02/2015",
				"-users", "500",
				"-privatekey", privateKeyFilePath};
		//@formatter:on

		entryPoint.execute(generateArgs);

		assertThat(outputStream.toString(), not(isEmptyString()));
		assertThat(errorStream.toString(), isEmptyString());

		File tempFile = File.createTempFile("license", ".tmp");
		tempFile.deleteOnExit();

		FileUtils.writeStringToFile(tempFile, outputStream.toString());

		//@formatter:off
		String[] verifyArgs = new String[] {
				"verify",
				"-license", tempFile.getAbsolutePath(),
				"-publickey", new File(this.getClass().getClassLoader().getResource("wrong_pub.pem").toURI())
				.getAbsolutePath()};
		//@formatter:on

		outputStream.reset();
		errorStream.reset();

		entryPoint.execute(verifyArgs);

		assertThat(errorStream.toString(), containsString("License is invalid - signature is incorrect!"));
	}

	@Test
	public void verifyNegativeMalformedLicenseTooFewBytes() throws Exception {
		File tempFile = File.createTempFile("license", ".tmp");
		tempFile.deleteOnExit();

		FileUtils.writeStringToFile(tempFile, "ZA==");

		//@formatter:off
		String[] verifyArgs = new String[] {
				"verify",
				"-license", tempFile.getAbsolutePath(),
				"-publickey", publicKeyFilePath};
		//@formatter:on

		outputStream.reset();
		errorStream.reset();

		entryPoint.execute(verifyArgs);

		assertThat(errorStream.toString(), containsString("License is maformed!"));
	}

	@Test
	public void verifyNegativeMalformedLicenseContentSizeNegative() throws Exception {
		File tempFile = File.createTempFile("license", ".tmp");
		tempFile.deleteOnExit();

		FileUtils.writeStringToFile(tempFile, "sdfasdf");

		//@formatter:off
		String[] verifyArgs = new String[] {
				"verify",
				"-license", tempFile.getAbsolutePath(),
				"-publickey", publicKeyFilePath};
		//@formatter:on

		outputStream.reset();
		errorStream.reset();

		entryPoint.execute(verifyArgs);

		assertThat(errorStream.toString(), containsString("License is maformed!"));
	}

	@Test
	public void verifyNegativeMalformedLicenseContentSizeTooLarge() throws Exception {
		File tempFile = File.createTempFile("license", ".tmp");
		tempFile.deleteOnExit();

		FileUtils.writeStringToFile(tempFile, "ZHNkZnNmYQ==");

		//@formatter:off
		String[] verifyArgs = new String[] {
				"verify",
				"-license", tempFile.getAbsolutePath(),
				"-publickey", publicKeyFilePath};
		//@formatter:on

		outputStream.reset();
		errorStream.reset();

		entryPoint.execute(verifyArgs);

		assertThat(errorStream.toString(), containsString("License is maformed!"));
	}

}
