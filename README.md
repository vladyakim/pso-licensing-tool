# README #

PSO Licensing Tool

### What is this repository for? ###

Licensing Tool used for PSO Products client license generation

### How it works? ###

* Clone the repository locally
* Go to **licensing-tool** folder. For example `cd ~/git/pso-licensing-tool/licensing-tool`
* You need to have above folder in your **PATH** variable. Example shown below:  
`export LICENSE_TOOL=~/git/pso-licensing-tool`  
`export PATH=$PATH:$LICENSE_TOOL/licensing-tool`  
*and you can verify*  
`echo $PATH`  
*path should be now set*
* Run **atlas-mvn clean install** if not already done
* Generate license by executing:  
`licensetool generate -org $1 -serverid $2 -from $3 -durationType $4 -durationPeriod $5 -users $6 -privatekey $7 > $8`  
*where parameter to supply are as follows:*  
`1 - Organization name, e.g. BotronSoft`
`2 - JIRA Server ID, e.g. BJRG-HTGY-IRMQ-VY65`
`3 - License start date in format dd/mm/yyyy`
`4 - Duration Type, e.g. MONTHLY`
`5 - Duration as integer, e.g. 3`
`6 - JIRA tier (currently not actually checked against JIRA tier license)`
`7 - Path where PSO Licensing Primary Key is stored`
`8 - Path where client license will be output`  
*or complete command would look something like:*  
`licensetool generate -org BotronSoft -serverid BJRG-HTGY-IRMQ-VY65 -from 23/08/2020 -durationType MONTHLY -durationPeriod 3 -users 500 -privatekey ~/.ssh/pso_licensing.pem > ~/Documents/license/Botron`
* То аpply license in PSO Tool there is REST resource **BASE_URL/rest/jmt/1.0/license** that:  
	**@GET** retrieves applied license details  
	**@POST** applies license  
	**@DELETE** deletes applied license

### Generating Primary/Public key pair ###

* Following commands are used to generate new key pair:  
	
**Generate Private key**  
	`openssl genpkey -out ~/.ssh/pso_licensing.pem -algorithm RSA -pkeyopt rsa_keygen_bits:4096`
	
**Generate Public key**
	`rsa -in pso_licensing.pem -pubout -out pso_licensing_public.crt`
	
**Apply new Public key in PSO tool**  
*In case new key pair is to be used, PSO tools need to be updated with the new public key which is set within each tool. For example in JMT this is set in:*  
`com.botronsoft.pso.jmt.license.CustomLicenseServiceImpl`  
`private static final String PUBLIC_KEY`
 